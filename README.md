# Podcaster

## Get started

```
$ npm install
$ npm start
```

Go to `http://localhost:8080`

## Architecture

PReact was used as the UI framework. Utility classes allowed keeping the CSS in the app to a minimum. 
The app involves a rudimentary design system using custum bars. As for the business logic, the domain is based on clean architecture
with two main models: podcast and episode. All repositories have an in-memory implementation, as required by the task. 

## Known issues
Because the episodes database is in-memory, reaching this page can only be done via the podcasts page.

## Final remarks
Testing and deployment aspects were out of the scope of the task, but I'd be happy to mention some ideas around these issues
during the interview.
