import {render} from 'preact'

import {App} from './Components/App/App'

render(<App />, document.getElementById('app'))
