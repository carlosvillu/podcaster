import {InvalidIDPodcastError} from '../../podcast/Errors/InvalidIDPodcastError'

export class IDValueObject {
  static validate({id}) {
    try {
      parseInt(id, 10)
    } catch {
      throw InvalidIDPodcastError.create(`Invalid ID(${id})`)
    }
  }

  static create({id}) {
    IDValueObject.validate({id})
    return new IDValueObject({id})
  }

  static generateWith({seed}) {
    return seed.split('').reduce(function(a, b) {
      a = (a << 5) - a + b.charCodeAt(0)
      return a & a
    }, 0)
  }

  static generate() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (
        c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
      ).toString(16)
    )
  }

  constructor({id}) {
    this._value = id
  }

  value() {
    return this._value
  }

  toJSON() {
    return {id: this._value}
  }
}
