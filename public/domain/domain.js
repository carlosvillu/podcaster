const interOP = (fn, name) => () => fn().then(mod => mod[name])
const UCs = {
  GetAllPodcastUseCase: interOP(() => import('./podcast/UseCases/GetAllPodcastUseCase'), 'GetAllPodcastUseCase'), // eslint-disable-line
  FindPodcastUseCase: interOP(() => import('./podcast/UseCases/FindPodcastUseCase'), 'FindPodcastUseCase'), // eslint-disable-line
  GetPodcastUseCase: interOP(() => import('./podcast/UseCases/GetPodcastUseCase'), 'GetPodcastUseCase'), // eslint-disable-line

  FindByPodcastEpisodeUseCase: interOP(() => import('./episode/UseCases/FindByPodcastEpisodeUseCase'), 'FindByPodcastEpisodeUseCase'), // eslint-disable-line
  GetEpisodeUseCase: interOP(() => import('./episode/UseCases/GetEpisodeUseCase'), 'GetEpisodeUseCase'), // eslint-disable-line
}

const unknowUCFactory = uc => async () => {
  return class UnknowUC {
    static create() {
      return new UnknowUC()
    }

    execute() {
      console.error(new Error(`Unkown UC(${uc})`))
    }
  }
}

export class Podcaster {
  static create() {
    return new Podcaster()
  }

  get(uc) {
    return {
      async execute() {
        const factory = UCs[uc] || unknowUCFactory(uc)
        const klass = await factory()
        return klass.create().execute(...arguments)
      }
    }
  }
}

export const domain = new Podcaster()
