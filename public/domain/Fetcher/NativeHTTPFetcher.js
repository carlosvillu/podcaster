import {HTTPFetcher} from './HTTPFetcher'

export class NativeHTTPFetcher extends HTTPFetcher {
  static create() {
    return new NativeHTTPFetcher()
  }

  async get({url, text, opts} = {opts: {}}) {
    const resp = await fetch(url, opts)

    if (text) {
      return resp.text()
    }
    return resp.json()
  }
}
