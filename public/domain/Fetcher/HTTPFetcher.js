export class HTTPFetcher {
  get() {
    throw new Error('[HTTPFetcher#get] should be implemented')
  }
}
