export class InvalidEpisodeError extends Error {
  static create(msg) {
    return new InvalidEpisodeError(msg)
  }
}
