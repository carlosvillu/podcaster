export class InvalidEpisodesError extends Error {
  static create(msg) {
    return new InvalidEpisodesError(msg)
  }
}
