export class GenericEpisodeError extends Error {
  static create(msg) {
    return new GenericEpisodeError(msg)
  }
}
