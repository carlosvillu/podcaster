import {IDValueObject} from '../../kernel/Models/IDValueObject'
import {InMemoryEpisodeRepository} from '../Respositories/InMemoryEpisodeRepository'

export class GetEpisodeUseCase {
  static create() {
    const repository = InMemoryEpisodeRepository.create()
    return new GetEpisodeUseCase({repository})
  }

  constructor({repository}) {
    this._repository = repository
  }

  async execute({id}) {
    const episode = await this._repository.find({
      episodeID: IDValueObject.create({id})
    })

    return episode.toJSON()
  }
}
