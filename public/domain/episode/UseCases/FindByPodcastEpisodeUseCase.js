import {IDValueObject} from '../../kernel/Models/IDValueObject'
import {InMemoryEpisodeRepository} from '../Respositories/InMemoryEpisodeRepository'

export class FindByPodcastEpisodeUseCase {
  static create() {
    const repository = InMemoryEpisodeRepository.create()
    return new FindByPodcastEpisodeUseCase({repository})
  }

  constructor({repository}) {
    this._repository = repository
  }

  async execute({podcastID}) {
    const episodes = await this._repository.findByPodcast({
      podcastID: IDValueObject.create({id: podcastID})
    })

    return episodes.toJSON()
  }
}
