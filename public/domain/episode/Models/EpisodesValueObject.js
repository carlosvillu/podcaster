import {InvalidEpisodesError} from '../Errors/InvalidEpisodesError'
import {EpisodeEntity} from './EpisodeEntity'

export class EpisodesValueObject {
  static validate({episodes}) {
    if (!Array.isArray(episodes)) {
      throw InvalidEpisodesError(`Invalid episodes(${episodes})`)
    }
  }

  static create({episodes}) {
    EpisodesValueObject.validate({episodes})
    return new EpisodesValueObject({
      episodes: episodes.map(EpisodeEntity.create)
    })
  }

  constructor({episodes}) {
    this._value = episodes
  }

  toJSON() {
    return {
      episodes: this._value.map(episode => episode.toJSON())
    }
  }
}
