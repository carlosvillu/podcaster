import {InvalidEpisodeError} from '../Errors/InvalidEpisodeError'
export class EpisodeEntity {
  static validate({id, title, duration, date, audio}) {
    if (!id || !audio) {
      throw InvalidEpisodeError.create(
        `Invalid episode id(${id}) title(${title}) duration(${duration}) date(${date}) audio(${JSON.stringify(audio)})` // eslint-disable-line
      )
    }
  }

  static create({id, title, duration, subtitle, date, audio}) {
    EpisodeEntity.validate({id, title, duration, date, audio, subtitle})
    return new EpisodeEntity({id, title, duration, date, audio, subtitle})
  }

  constructor({id, title, duration, date, audio, subtitle}) {
    this._id = id
    this._title = title
    this._duration = duration
    this._date = date
    this._audio = audio
    this._subtitle = subtitle
  }

  toJSON() {
    return {
      id: this._id,
      title: this._title,
      duration: this._duration,
      date: this._date,
      audio: this._audio,
      subtitle: this._subtitle
    }
  }
}
