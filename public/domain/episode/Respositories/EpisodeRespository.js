export class EpisodeRepository {
  find() {
    throw new Error(`[EpisodeRepository#find] should be implemented`)
  }

  findByPodcast() {
    throw new Error(`[EpisodeRepository#findByPodcast] should be implemented`)
  }
}
