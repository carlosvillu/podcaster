import {EpisodeEntity} from '../Models/EpisodeEntity'
import {EpisodesValueObject} from '../Models/EpisodesValueObject'
import {IDValueObject} from '../../kernel/Models/IDValueObject'
import {EpisodeRepository} from './EpisodeRespository'
import {HTTPEpisodeDataSource} from '../DataSource/HTTPEpisodeDataSource'

/* global __EPISODE_DB__ */

window.__EPISODE_DB__ = {__podcast_index__: {}}

export class InMemoryEpisodeRepository extends EpisodeRepository {
  static create() {
    const datasource = HTTPEpisodeDataSource.create()
    return new InMemoryEpisodeRepository({datasource})
  }

  constructor({datasource}) {
    super(...arguments)
    this._datasource = datasource
  }

  async findByPodcast({podcastID}) {
    const db = await this._populateDBWithEpisodesFromPodcast_({podcastID})
    const episodesIDX = db.__podcast_index__[podcastID.value()]

    return EpisodesValueObject.create({episodes: episodesIDX.map(id => db[id])})
  }

  async find({episodeID}) {
    const db = await this._db_()
    return EpisodeEntity.create(db[episodeID.value()])
  }

  async _db_() {
    return window.__EPISODE_DB__
  }

  async _populateDBWithEpisodesFromPodcast_({podcastID}) {
    const episodes = await this._datasource.dataFor({podcastID})
    episodes.reduce((acc, episode) => {
      if (!episode?.audio?.src) {
        return acc
      }

      const id = IDValueObject.generateWith({seed: episode.audio.src})

      if (acc[id]) {
        return acc
      }

      acc[id] = {
        id,
        ...episode
      }
      const currentIDX = acc.__podcast_index__[podcastID.value()] ?? []
      acc.__podcast_index__[podcastID.value()] = [...currentIDX, id]

      return acc
    }, window.__EPISODE_DB__)

    return window.__EPISODE_DB__
  }
}
