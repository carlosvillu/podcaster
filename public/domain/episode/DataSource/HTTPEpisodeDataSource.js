import {GenericEpisodeError} from '../Errors/GenericEpisodeError'
import {NativeHTTPFetcher} from '../../Fetcher/NativeHTTPFetcher'

const ONE_DAY = 24 * 60 * 60 * 1000
const CACHE_KEY = '__HTTPEpisodeDataSource_CACHE__'
window[CACHE_KEY] = window[CACHE_KEY] || {}

export class HTTPEpisodeDataSource {
  static create() {
    return new HTTPEpisodeDataSource({
      fetcher: NativeHTTPFetcher.create()
    })
  }

  constructor({fetcher}) {
    this._fetcher = fetcher
  }

  async dataFor({podcastID}) {
    try {
      const now = Date.now()
      const previous = window[CACHE_KEY][podcastID.value()]?.timestamp ?? now
      const delta = now - previous

      if (delta > ONE_DAY) {
        return window[CACHE_KEY][podcastID.value()].episodes
      }

      const podcastJSON = await this._fetcher.get({
        url: `https://cors-anywhere.herokuapp.com/https://itunes.apple.com/lookup?id=${podcastID.value()}`
      })
      const feedURL = podcastJSON.results[0].feedUrl
      const xml = await this._fetcher.get({
        url: `https://cors-anywhere.herokuapp.com/${feedURL}`,
        text: true
      })
      const rss = new DOMParser().parseFromString(xml, 'text/xml')
      const items = Array.from(rss.getElementsByTagName('item'))
      const episodes = items.map(item => {
        return {
          title: item.querySelector('title')?.textContent ?? '',
          duration: parseInt(item.getElementsByTagName('itunes:duration')?.[0]?.textContent ?? 0), // eslint-disable-line
          date: new Date(item.querySelector('pubDate')?.textContent ?? null),
          subtitle: item.getElementsByTagName('itunes:subtitle')?.[0]?.textContent ?? '', // eslint-disable-line
          audio: {
            src: item.querySelector('enclosure')?.getAttribute('url') ?? null,
            length: item.querySelector('enclosure')?.getAttribute('length') ?? 0, // eslint-disable-line
            type: item.querySelector('enclosure')?.getAttribute('type') ?? ''
          }
        }
      })

      window[CACHE_KEY][podcastID.value()] = {timestamp: now, episodes}
      return episodes
    } catch (error) {
      throw GenericEpisodeError.create(error.message)
    }
  }
}
