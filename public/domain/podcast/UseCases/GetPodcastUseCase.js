import {InMemoryPodcastRepository} from '../Repositories/InMemoryPodcastRepository'
import {IDValueObject} from '../../kernel/Models/IDValueObject'

export class GetPodcastUseCase {
  static create() {
    return new GetPodcastUseCase({
      repository: InMemoryPodcastRepository.create()
    })
  }

  constructor({repository}) {
    this._repository = repository
  }

  async execute({id}) {
    const podcasts = await this._repository.one({
      id: IDValueObject.create({id})
    })

    return podcasts.toJSON()
  }
}
