import {InMemoryPodcastRepository} from '../Repositories/InMemoryPodcastRepository'

export class GetAllPodcastUseCase {
  static create() {
    return new GetAllPodcastUseCase({
      repository: InMemoryPodcastRepository.create()
    })
  }

  constructor({repository}) {
    this._repository = repository
  }

  async execute() {
    const podcasts = await this._repository.all()

    return podcasts.toJSON()
  }
}
