import {InMemoryPodcastRepository} from '../Repositories/InMemoryPodcastRepository'
import {QueryValueObject} from '../Models/QueryValueObject'

export class FindPodcastUseCase {
  static create() {
    return new FindPodcastUseCase({
      repository: InMemoryPodcastRepository.create()
    })
  }

  constructor({repository}) {
    this._repository = repository
  }

  async execute({query}) {
    const podcasts = await this._repository.find({
      query: QueryValueObject.create({query})
    })

    return podcasts.toJSON()
  }
}
