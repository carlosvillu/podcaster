import {NotNeedRevalidateDataPodcastError} from '../Errors/NotNeedRevalidateDataPodcastError'
import {NativeHTTPFetcher} from '../../Fetcher/NativeHTTPFetcher'

const ONE_DAY = 24 * 60 * 60 * 1000
const PREVIOUS_TIMESTAMP_KEY = '__HTTPPodcastDataSource_PREVIOUS_TS__'

export class HTTPPodcastDataSource {
  static create() {
    return new HTTPPodcastDataSource({
      fetcher: NativeHTTPFetcher.create()
    })
  }

  constructor({fetcher}) {
    this._fetcher = fetcher
  }

  async data() {
    const now = Date.now()
    const previous = parseInt(window[PREVIOUS_TIMESTAMP_KEY] || now, 10)
    const delta = now - previous

    if (delta > ONE_DAY) {
      throw NotNeedRevalidateDataPodcastError.create(`Last update was ${delta}`)
    }

    const json = await this._fetcher.get({
      url:
        'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json'
    })

    window[PREVIOUS_TIMESTAMP_KEY] = JSON.stringify(now)
    return json?.feed?.entry ?? []
  }
}
