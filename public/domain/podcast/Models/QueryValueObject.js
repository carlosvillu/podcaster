export class QueryValueObject {
  static create({query = ''}) {
    return new QueryValueObject({query})
  }

  constructor({query}) {
    this._value = query
  }

  value() {
    return this._value
  }

  isEmpty() {
    return this._value === ''
  }

  toJSON() {
    return {query: this._value}
  }
}
