import {InvalidEntityPodcastError} from '../Errors/InvalidEntityPodcastError'
import {NotFoundPodcastError} from '../Errors/NotFoundPodcastError'
import {PodcastEntity} from '../Models/PodcastEntity'

const EMPTY = 0

export class PodcastsValueObject {
  static validate({podcasts}) {
    if (!Array.isArray(podcasts) || podcasts.length === EMPTY) {
      throw InvalidEntityPodcastError.create(
        `[PodcastsValueObject.validate] podcasts(${podcasts})`
      )
    }
  }

  static create({podcasts}) {
    return new PodcastsValueObject({
      podcasts: podcasts.map(PodcastEntity.create)
    })
  }

  constructor({podcasts}) {
    this._value = podcasts
  }

  findBy({query}) {
    if (query.isEmpty()) {
      return PodcastsValueObject.create(this.toJSON())
    }

    const matchValue = this._value.filter(
      podcast =>
        podcast
          .name()
          .toLowerCase()
          .includes(query.value().toLowerCase()) ||
        podcast
          .title()
          .toLowerCase()
          .includes(query.value().toLowerCase())
    )

    const matchPodcasts = PodcastsValueObject.create({
      podcasts: matchValue.map(podcast => podcast.toJSON())
    })

    return matchPodcasts
  }

  podcastBy({id}) {
    const podcast = this._value.find(podcast => podcast.id() === id.value())

    if (!podcast) {
      throw NotFoundPodcastError.create(
        `Not found podcast with id(${id.value()})`
      )
    }

    return PodcastEntity.create(podcast.toJSON())
  }

  toJSON() {
    return {
      podcasts: this._value.map(entity => entity.toJSON())
    }
  }
}
