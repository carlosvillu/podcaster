import {InvalidEntityPodcastError} from '../Errors/InvalidEntityPodcastError'

export class PodcastEntity {
  static validate({id, name, title} = {}) {
    if (!id || !name || !title) {
      throw InvalidEntityPodcastError.create(
        `[PodcastEntity.validate] id(${id}) title(${title}) name(${name})`
      )
    }
  }

  static create({id, name, image, summary, title}) {
    PodcastEntity.validate({id, name, title})

    return new PodcastEntity({id, name, image, summary, title})
  }

  constructor({id, name, image, summary, title}) {
    this._id = id
    this._name = name
    this._image = image
    this._summary = summary
    this._title = title
  }

  name() {
    return this._name
  }

  title() {
    return this._title
  }

  id() {
    return this._id
  }

  toJSON() {
    return {
      id: this._id,
      name: this._name,
      image: this._image,
      summary: this._summary,
      title: this._title
    }
  }
}
