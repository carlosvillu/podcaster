export class NotFoundPodcastError extends Error {
  static create(msg) {
    return new NotFoundPodcastError(msg)
  }
}
