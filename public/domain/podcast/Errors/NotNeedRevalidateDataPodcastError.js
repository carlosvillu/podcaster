export class NotNeedRevalidateDataPodcastError extends Error {
  static create(msg) {
    return new NotNeedRevalidateDataPodcastError(msg)
  }
}
