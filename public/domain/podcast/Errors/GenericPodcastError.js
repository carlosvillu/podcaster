export class GenericPodcastError extends Error {
  static create(msg) {
    return new GenericPodcastError(msg)
  }
}
