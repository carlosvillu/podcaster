export class InvalidIDPodcastError extends Error {
  static create(msg) {
    return new InvalidIDPodcastError(msg)
  }
}
