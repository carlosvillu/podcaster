export class InvalidEntityPodcastError extends Error {
  static create(msg) {
    return new InvalidEntityPodcastError(msg)
  }
}
