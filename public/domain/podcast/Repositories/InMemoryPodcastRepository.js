// await (await fetch('https://cors-anywhere.herokuapp.com/https://itunes.apple.com/lookup?id=77407482')).json()
// xml = await (await fetch('https://cors-anywhere.herokuapp.com/http://bellobard.libsyn.com/rss')).text()
// rss = new DOMParser().parseFromString(xml,"text/xml");
// Array.from(rss.getElementsByTagName('item'))

/* global __PODCAST_DB__ */
import {PodcastRepository} from './PodcastRepository'
import {PodcastsValueObject} from '../Models/PodcastsValueObject'

import {NotNeedRevalidateDataPodcastError} from '../Errors/NotNeedRevalidateDataPodcastError'
import {GenericPodcastError} from '../Errors/GenericPodcastError'
import {HTTPPodcastDataSource} from '../DataSource/HTTPPodcastDataSource'

window.__PODCAST_DB__ = {}

export class InMemoryPodcastRepository extends PodcastRepository {
  static create() {
    return new InMemoryPodcastRepository({
      datasource: HTTPPodcastDataSource.create()
    })
  }

  constructor({datasource}) {
    super()
    this._datasource = datasource
  }

  async all() {
    const db = await this._db_()
    const list = PodcastsValueObject.create({podcasts: Object.values(db)})
    return list
  }

  async one({id}) {
    const db = await this._db_()
    const list = PodcastsValueObject.create({podcasts: Object.values(db)})
    return list.podcastBy({id})
  }

  async find({query}) {
    const list = await this.all()

    const results = list.findBy({query})
    return results
  }

  async _db_() {
    try {
      const data = await this._datasource.data()
      data.reduce((acc, entry) => {
        acc[entry.id.attributes['im:id']] = {
          id: entry.id.attributes['im:id'],
          name: entry['im:name'].label,
          image: {
            s: entry['im:image'][0]?.label,
            l: entry['im:image'][1]?.label,
            xl: entry['im:image'][2]?.label
          },
          summary: entry.summary.label,
          title: entry.title.label
        }

        return acc
      }, __PODCAST_DB__)

      return __PODCAST_DB__
    } catch (err) {
      if (!(err instanceof NotNeedRevalidateDataPodcastError)) {
        throw GenericPodcastError.create(
          `[InMemoryPodcastRepository#_validate] generic error ${err.message}`
        )
      }
      return __PODCAST_DB__
    }
  }
}
