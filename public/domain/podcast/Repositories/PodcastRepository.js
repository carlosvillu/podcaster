export class PodcastRepository {
  all() {
    throw new Error('[PodcastRepository#all] should be implemented')
  }

  one() {
    throw new Error('[PodcastRepository#one] should be implemented')
  }

  find() {
    throw new Error('[PodcastRepository#find] should be implemented')
  }
}
