const Paper = ({children, className} = {className: ''}) => {
  return <div className={`bxsh-paper ${className}`}>{children}</div>
}

export {Paper}
