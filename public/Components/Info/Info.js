const Info = ({poster, title, author, description}) => {
  return (
    <>
      <div className="as_center bdb-primary-d-3-s w-full d_flex jc_center">
        <img
          className="bdrs-s mb-xl mt-xl"
          alt="img"
          width={170}
          height={170}
          loading="lazy"
          src={poster}
        />
      </div>
      <div className="bdb-primary-d-3-s pb-xl pt-xl">
        <b className="fz-xs d_block">{title}</b>
        <i className="fz-xs">by {author}</i>
      </div>
      <div className=" pb-xl pt-xl">
        <b className="fz-xs d_block">Description:</b>
        <i className="fz-xs">{description}</i>
      </div>
    </>
  )
}

export {Info}
