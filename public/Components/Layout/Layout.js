import {Footer} from '../Footer/Footer'
import {Header} from '../Header/Header'

const Layout = ({children}) => {
  return (
    <section className="d_grid gta-holy-layout gtc-holy-layout gtr-holy-layout gg-holy-layout h_100vh">
      <header className="ga_header h-s bdb-primary-d-3-s">
        <Header />
      </header>
      <main className="ga_content">{children}</main>
      <footer className="ga_footer h-s ">
        <Footer />
      </footer>
    </section>
  )
}

export {Layout}
