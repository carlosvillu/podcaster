import Router from 'preact-router'
import AsyncRoute from 'preact-async-route'

import {Layout} from '../Layout/Layout'

const loadHome = () => import('../Home/Home.js').then(m => m.Home)
const loadPodcastDetail = () => import('../PodcastDetail/PodcastDetail').then(m => m.PodcastDetail) // eslint-disable-line
const loadEpisodeDetail = () => import('../EpisodeDetail/EpisodeDetail').then(m => m.EpisodeDetail) // eslint-disable-line

const App = () => {
  return (
    <Layout>
      <Router>
        <AsyncRoute path="/" getComponent={loadHome} />
        <AsyncRoute path="/podcast/:id" getComponent={loadPodcastDetail} />
        <AsyncRoute
          path="/podcast/:podcastID/episode/:episodeID"
          getComponent={loadEpisodeDetail}
        />
      </Router>
    </Layout>
  )
}

export {App}
