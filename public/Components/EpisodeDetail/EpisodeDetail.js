import {Info} from '../Info/Info'
import {Paper} from '../Paper/Paper'
import {Loading} from '../Loading/Loading'
import {useState, useEffect} from 'preact/hooks'

import {domain} from '../../domain/domain'

const EpisodeDetail = ({episodeID, podcastID}) => {
  const [podcast, setPodcast] = useState()
  const [episode, setEpidode] = useState()
  useEffect(() => {
    async function populateState() {
      const podcast = await domain
        .get('GetPodcastUseCase')
        .execute({id: podcastID})

      const episode = await domain
        .get('GetEpisodeUseCase')
        .execute({id: episodeID})
      setPodcast(podcast)
      setEpidode(episode)
    }

    populateState()
  }, [episodeID, podcastID])

  if (!podcast || !episode) {
    return <Loading />
  }

  return (
    <div className="mt-xl h-full d_grid gcg-s gta-detailPodcast gtc-detailPodcast gtr-detailPodcast">
      <section className="ga_infoPodcast ">
        <Paper className="d_flex fxd_column pr-xl pl-xl">
          <Info
            poster={podcast.image.xl}
            title={podcast.title}
            author={podcast.title}
            description={podcast.summary}
          />
        </Paper>
      </section>
      <section className="ga_headerPodcast h-full">
        <Paper className="h-full w-full d_flex fxd_column ai_center pl-xl pr-xl">
          <b className="mb-l mt-l as_end">{episode.title}</b>
          <i>{episode.subtitle}</i>
          <audio controls className="w-full mt-xl mb-xl">
            <source src={episode.audio.src} type={episode.audio.type} />
          </audio>
        </Paper>
      </section>
    </div>
  )
}

export {EpisodeDetail}
