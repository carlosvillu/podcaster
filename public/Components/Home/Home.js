import {Link} from 'preact-router'
import {useState, useEffect} from 'preact/hooks'

import {Budget} from '../Budget/Budget'
import {MiniInfo} from '../MiniInfo/MiniInfo'
import {Loading} from '../Loading/Loading'

import {domain} from '../../domain/domain'
import {i18n} from '../../i18n/i18n'

const Home = () => {
  const [query, setQuery] = useState('')
  const [podcasts, setPodcasts] = useState([])
  useEffect(() => {
    domain
      .get('FindPodcastUseCase')
      .execute({query})
      .then(resp => {
        setPodcasts(resp.podcasts)
      })
  }, [query])

  if (podcasts.length === 0 && query === '') {
    return <Loading />
  }

  return (
    <div>
      <div className="h-s d_flex ai_center jc_flex-end">
        <Budget>{podcasts.length}</Budget>
        <input
          value={query}
          onInput={evt => {
            setQuery(evt.target.value)
          }}
          className="h-xs ml-l p-xl w-third bdrs-s bd-primary-d-3-s"
          type="text"
          placeholder={i18n.t('HOME_INPUT_PLACEHOLDER')}
        />
      </div>
      <div className="d_grid gtc_3 ji_center">
        {podcasts.map(podcast => {
          return (
            <Link
              key={podcast.id}
              href={`/podcast/${podcast.id}`}
              className="td_none c-primary-d-4"
            >
              <MiniInfo
                author={podcast.name}
                title={podcast.title}
                poster={podcast.image.xl}
              />
            </Link>
          )
        })}
      </div>
    </div>
  )
}

export {Home}
