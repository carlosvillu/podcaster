import {Info} from '../Info/Info'
import {Paper} from '../Paper/Paper'
import {Loading} from '../Loading/Loading'
import {Link} from 'preact-router'
import {useState, useEffect} from 'preact/hooks'

import {domain} from '../../domain/domain'

const PodcastDetail = ({id}) => {
  const [podcast, setPodcast] = useState()
  const [episodes, setEpidodes] = useState([])
  useEffect(() => {
    async function populateState() {
      const podcast = await domain.get('GetPodcastUseCase').execute({id})
      const {episodes} = await domain
        .get('FindByPodcastEpisodeUseCase')
        .execute({podcastID: id})
      setPodcast(podcast)
      setEpidodes(episodes)
    }

    populateState()
  }, [id])

  if (!podcast) {
    return <Loading />
  }

  return (
    <div className="mt-xl h-full d_grid gcg-s gta-detailPodcast gtc-detailPodcast gtr-detailPodcast">
      <section className="ga_infoPodcast ">
        <Paper className="d_flex fxd_column pr-xl pl-xl">
          <Info
            poster={podcast.image.xl}
            title={podcast.title}
            author={podcast.title}
            description={podcast.summary}
          />
        </Paper>
      </section>
      <section className="ga_headerPodcast h-l">
        <Paper className="h-s w-full d_flex ai_center pl-xl pr-xl">
          Episodies {episodes.length}
        </Paper>
      </section>
      <section className="ga_episodiesPodcast">
        <Paper className="h-full w-full pl-xl pr-xl">
          <table className="w-full h-full">
            <thead>
              <tr>
                <th className="ta_left">Title</th>
                <th className="">Date</th>
                <th className="">Duration</th>
              </tr>
            </thead>
            <tbody>
              {episodes
                .filter(episode => episode.audio.src)
                .map(episode => {
                  return (
                    <tr key={episode.id} className="h-s">
                      <td className="w-two-third ov_hidden tov_ellipsis">
                        <Link
                          href={`/podcast/${id}/episode/${episode.id}`}
                          className="td_none"
                        >
                          {episode.title}
                        </Link>
                      </td>
                      <td className="w-s">
                        {episode.date.toLocaleDateString()}
                      </td>
                      <td className="w-s ta_center">{episode.duration}</td>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </Paper>
      </section>
    </div>
  )
}

export {PodcastDetail}
