import {Paper} from '../Paper/Paper'

const MiniInfo = ({poster, author, title}) => {
  return (
    <div className="h-xxxl w-xxl d_flex jc_flex-end fxd_column ai_center trs_transform-l hover--trf_scale-s cur_pointer">
      <img
        src={poster}
        alt={title}
        width="150"
        height="150"
        loading="lazy"
        className="bdrs-circle w-xl pos_relative t-s"
      />
      <Paper className="fxg_2 d_flex fxd_column jc_flex-end ai_center w-full">
        <p className="m-none fz-xs whs-nowrap tov_ellipsis ov_hidden w-two-third">
          {title}
        </p>
        <p className="c-primary-d-3 fz-xs whs-nowrap tov_ellipsis ov_hidden w-two-third">
          author: {author}
        </p>
      </Paper>
    </div>
  )
}

export {MiniInfo}
