const Header = () => {
  return (
    <div className="h-full w-full d_flex ai_center">
      <a href="/" className="c-secondary td_none">
        Podcaster
      </a>
    </div>
  )
}

export {Header}
