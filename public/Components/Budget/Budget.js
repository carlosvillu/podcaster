const Budget = ({children}) => {
  return (
    <span className="bgc-secondary p-s bdrs-s c-primary fz-xs">{children}</span>
  )
}

export {Budget}
