import {esES} from './es-ES.js'
import {enGB} from './en-GB.js'

const langs = {esES, enGB}

export const i18n = {
  lang: 'enGB',
  t(literal) {
    return langs[this.lang][literal] ?? `NOT FOUND LITERAL(${literal})`
  }
}
